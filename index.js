const express = require ('express')
const app = express()
const port = 4000
const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/users_db', {useNewUrlParser: true});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  'We are connected!!'
});

const greetingRouter = require('./route/greeting')
const userRouter = require('./route/users')

app.get('/', (req, res) => {
    res.send ('Bienvenue sur notrre super site')
})

// app.get('/greeting/:prenom', (req, res) => { // :prenom est une variable dans la requete, on peut y stocké une valeur
//     res.send (`message:Salut ${req.params.prenom}`)
// })

// app.route('/greeting')
// .get((req, res)=> {
//     res.send(`OK!`)
// })

app.use(express.urlencoded({extended: true}))
app.use(express.json())
app.use('/greeting', greetingRouter)
app.use('/users', userRouter)

// .post((req, res)=> {
//     res.send(`Add something`)
// })

// .put((req, res)=> {
//     res.send(`Update something`)
// })

app.listen(port, ()=> {
    `Mon serveur marche bien! sur ce port : ${port}`
})