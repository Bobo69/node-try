const express = require('express')
const userRouter = express.Router()
// const users = require('../users.json')
const User = require('../model/user')


// userRouter.get('/', (req, res) => {
//     res.json(users)
// })

userRouter.get('/', (req, res) => {
    User.find({}, (err, users) => {
        if (err) console.error(err)
        res.json(users)
    })
})

userRouter.post('/new', (req, res) => {
    const newUser = new User(req.body)
    // {}
    // users.push(newUser)
    newUser.save((err, newUser) => {
        if (err) console.error(err)
        res.send({msg: `User added`, newUser})
    })
    // res.send(`msg:new user added !!`, users)
})

module.exports = userRouter;